# Exercice 5 : Backend

Pour le moment l'état de votre infrastructure est stocké dans un fichier `terraform.tfstate` à la racine du projet.

Vous pouvez consulter son contenu: `vim terraform.tfstate`
C'est son contenu que renvoit aussi la commande `terraform show`

Si on supprime ce fichier, le projet ne peut plus gérer l'état de l'infrastructure provisionné Il agira comme rien n'existait, ce qui peut poser des problèmes de doublons. Il en sera de même si quelqu'un exécutait le projet à partir d'une autre machine, ou même d'un autre répertoire.

Pour sécuriser le stockage du fichier d'état et faciliter la collaboration, nous allons mettre en place un backend qui s'appuie sur un bucket S3.

Vous pouvez consulter la [documentation officielle](https://www.terraform.io/docs/backends/types/s3.html).

Ouvrir le fichier `main.tf` pour modifier la configuration de terraform:

```bash
# Configure Terraform
terraform {
  ...

  # Backend on AWS
  backend "s3" {
    # Bucket name created before
    bucket               = "training-tf-backend"
    key                  = "<YOUR_NAME>/terraform.tfstate"
    dynamodb_table       = "training-tf-lock"
  }
}
```

*Le bucket et la table Dynamodb ont été créés dans le cadre de la préparation de la formation, au même titre que vu précédemment pour le VPC.*

Bien remplacer `<YOUR_NAME>` par votre nom, c'est important pour que chacun ait un répertoire différent dans le bucket.

Malheureusement on ne peut pas utiliser de variable dans la configuration du backend, il faut le mettre en dur.

Quand c'est fait, relancer la commande `terraform init`, terraform devrait vous proposer de migrer votre fichier d'état actuel vers le nouveau backend.
