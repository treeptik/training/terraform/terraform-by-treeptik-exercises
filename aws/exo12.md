# Exercice 12 : Destruction de l'infrastructure

Détruire l'infrastructure avec la commande `terraform destroy`.

Vérifier avec `terraform show` qu'il ne reste plus rien.

Constater que le fichier d'inventaire `./hosts`, en tant que ressource du projet même si c'est un type particulier, a disparu du projet aussi au même titre que les autres ressources d'infrastructure.
