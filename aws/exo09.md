# Exercice 9 : Cycle de vie

Dans cet exercice, nous allons manipuler différentes commandes qui nous permettent d'agir sur le cycle de vie des ressources dans notre projet.

## terraform state mv

Utiliser la commande `terraform show` pour consulter le fichier d'état de votre projet.

Repérer dedans la key pair dont l'identifiant terraform doit être `module.ec2.aws_key_pair.aws_ec2_key`. Noter la valeur de sa propriété `key_name`, nous en aurons besoin pour l'import.

Nous souhaitons changer l'identifiant de cette ressource dans notre projet Terraform, sans devoir la recréer pour autant.

Actuellement elle est identifiée par `module.ec2.aws_key_pair.aws_ec2_key`

Changer son identifiant avec `terraform state mv` pour qu'il devienne `module.ec2.aws_key_pair.public_key` dans le fichier d'état.

Faire un `terraform plan` et étudier les modifications proposées.

Il faut aligner ce nouvel identifiant dans le code:

- pour l'objet aws_key_pair lui-même
- pour chaque utilisation de l'objet

Refaire un `terraform plan`, aucune modification ne devrait être nécéssaire.

## terraform state rm

Nous allons maintenant retirer cette ressource de notre fichier d'état, comme si nous ne voulions plus le gérer dans notre projet:

```bash
$ terraform state rm module.ec2.aws_key_pair.public_key
Removed module.ec2.aws_key_pair.public_key
Successfully removed 1 resource instance(s).
```

Que se passe-t-il quand on fait un `terraform plan` ?

Terraform propose de recréer la ressource. Elle existe toujours sur AWS, mais notre projet n'a aucun moyen de le savoir car elle n'apparaît plus dans son fichier d'état.

## terraform import

Pour la rajouter nous allons utiliser la commande d'import:

```bash
$ terraform import module.ec2.aws_key_pair.public_key <YOUR_KEY_NAME>
module.ec2.aws_key_pair.public_key: Importing from ID "aws_ec2_key-tpe"...
module.ec2.aws_key_pair.public_key: Import prepared!
  Prepared aws_key_pair for import
module.ec2.aws_key_pair.public_key: Refreshing state... [id=public_key]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.
```

Chaque type de ressource possède son propre format pour la commande d'import, il faut se référer à la documentation pour la connaître. Par exemple, pour les ressources `aws_key_pair` c'est [ici](https://www.terraform.io/docs/providers/aws/r/key_pair.html#import).

Dans le cas précis d'un objet `aws_key_pair`, il va être modifié après l'import au prochain `terraform apply`. Mais dans l'ensemble le principe d'import permet d'ajouter des ressources existantes au projet sans qu'elle ne soient impactées.

Faites un `terraform apply` pour appliquer les changements.

## terraform taint

Nous ressource `aws_instance.vm` du module crée plusieurs instance EC2 grâce à la propriété `count`.

Comment faire si je souhaite recréer une seule des VM sans toutes les recréer ?

On utilise alors commande `terraform taint` pour flagger précisemment la ressources à recréer. Pour cela, on récupère l'identifiant précis dans le fichier d'état, par exemple `module.ec2.aws_instance.vm[0]`.

```bash
$ terraform taint module.ec2.aws_instance.vm[0]
Resource instance module.ec2.aws_instance.vm[0] has been marked as tainted.
```

Les modifications seront apportées au prochain `terraform apply`.
