# Exercice 1 : Mise en oeuvre

## Serveur d'admin

Vérifier que vous arrivez à vous connecter sur le serveur d'admin avec le compte `training`.
Demandez le mot de passe au formateur s'il ne vous l'a pas donné.

## Vérifier l'environnement

Commencer par vérifier que l'environnement est correctement configuré.

Consulter le fichier de variable `~/training-variable.txt`.

Vérifier que le projet d'exercice de cette formation est bien présent dans `~/terraform-by-treeptik-exercises`.

Vérifier que Terraform est bien installé en testant la commande `terraform`. En profiter pour consulter les différentes commandes de la CLI Terraform.

## Initialiser le projet

### Exporter la secret key

Mettre en place l'export des variables d'environnement suivantes :

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION`

Pour rappel, l'export des variables d'environnement sous Linux se fait de la manière suivante:

```bash
export AWS_ACCESS_KEY_ID=<YOUR_KEY>
export AWS_SECRET_ACCESS_KEY=<YOUR_AWS_SECRET_ACCESS_KEY>
export AWS_DEFAULT_REGION=<YOUR_AWS_DEFAULT_REGION>
```

Ajouter ces exports en fin du fichier `.profile` afin que les variables soient présentes à chaque ouverture de session. N'oubliez pas de sourcer le fichier après modification `source ~/.profile`.

Vous pouvez vérifier qu'elles bien présentes dans l'environnement avec la commande `env | grep AWS`.

Pour vérifier que les credentials fonctionnent, vous pouvez consulter votre identité avec l'Awscli: `aws sts get-caller-identity`.

### Créer le projet

Créer un répertoire `training` pour accueillir vos travaux, se déplacer dedans.

Créer un fichier `main.tf` avec la configuration de Terraform incluant les propriétés `required_providers` et `required_version`.

- Indiquer que le projet utilise une version de Terraform supérieure à `0.15.0`.
  - Si besoin consulter [la documentation officielle](https://www.terraform.io/docs/configuration/terraform.html#specifying-a-required-terraform-version).

- Nous allons utiliser le provider `aws` avec une version supérieure à `3.0`. 
  - Consulter [la documentation officielle](https://www.terraform.io/docs/providers/aws/index.html) pour vous aider.
  - On s'appuie sur les variables d'environnement pour les credentials AWS.

Initialiser le projet avec la commande `init` de la CLI Terraform.
Regardez bien la sortie console pour compendre ce qui s'est passé.

Vous pouvez tester différentes commandes de la CLI Terraform pour vous familiariser avec:

- `validate`
- `plan`
- `apply`
