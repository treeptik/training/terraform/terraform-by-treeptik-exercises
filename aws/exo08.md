# Exercice 8 : Création de workspace

Terraform démarre avec un seul workspace nommé "default". Ce workspace est spécial à la fois parce qu'il est celui par défaut et aussi parce qu'il ne peut jamais être supprimé. Si vous n'avez jamais utilisé explicitement de workspaces, alors vous avez toujours travaillé sur le workspace "default".

Les workspaces sont gérés avec l'ensemble des commandes `terraform workspace`:

* Pour créer un nouveau workspace et y basculer, vous pouvez utiliser `terraform workspace new <NAME>`
* Pour changer d'espace de travail, vous pouvez utiliser `terraform workspace select <NAME>`

Par exemple pour créer un nouveau workspace:

``` bash
$ terraform workspace new bar
Created and switched to workspace "bar"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.
```

Dans notre cas, nous allons créer nouvelle variable de type `map` qui portera une valeur différente pour chaque environnement.

Ajouter la variable suivante dans le fichier `variables.tf` à la racine du projet:

``` bash
variable "env_instance_number" {
  type        = map(number)
  description = "(Optional) Number of instance in the current environment"

  default = {
    "default"  = 2
    "dev"      = 3
    "staging"  = 3
    "prod"     = 4
  }
}
```

Dans le fichier `main.tf`, définissez les variables locales à utiliser dans le projet:

``` bash
locals {
  environment     = terraform.workspace
  instance_number = lookup(var.env_instance_number, terraform.workspace)
}
```

On utilisera cette variable `instance_number` dans le projet en remplacement de `var.VM_NUMBER`, elle portera la valeur spécifique au workspace courant.

Modifier ensuite dans l'appel au module `ec2` la variable :

``` bash
# Qui fait référence à la variable local
VM_NUMBER = local.instance_number
```

Créer ensuite vos environnements:

``` bash
terraform workspace new dev
terraform workspace new staging
terraform workspace new prod
```

Pour lister vos différents workspace, vous pouvez utiliser la commande `terraform workspace list`:

```bash
$ terraform workspace list
  default
  dev
* prod
  staging
```

L'étoile indique que nous sommes actuellement positionné sur le workspace `prod`, le dernier workspace créé. Revenez sur le workspace `default`:
`terraform workspace select default`

Utiliser `plan` et `apply` pour vérifier que tout fonctionne correctement pour le workspace default.

Sélectionner maintenant l'environnements de dev avec `terraform workspace select dev` et jouer la commande `terraform plan`.

Remarquer que le nombre d'instances à créer est de 3.

Sélectionner maintenant l'environnement de prod  avec `terraform workspace select prod` et jouer la commande `terraform plan`.

Remarquer que le nombre d'instances à créer est de 4.

Revenez sur le workspace `default` pour la suite: `terraform workspace select default`

Si possible, aller voir dans le bucket S3 comment sont organisés les fichiers d'état quand on utilise d'autres workspaces que celui par défaut.
