# Exercice 4 : Les boucles

## Count

Dans cet exercice nous allons ajouter une seconde VM sur le même modèle que la première.

Ajouter la nouvelle variable `VM_NUMBER` dans le fichier de variables avec `2` comme valeur par défaut.

Dans le fichier `vm.tf`, ajouter la propriété count à la ressource `aws_instance` en utilisant la variable `VM_NUMBER` comme valeur.

Utiliser aussi l'iterateur du *count* pour rendre dynamique le nom des VMs, pour ce faire remplacer la propriété `Name` comme ceci :

```yaml
# Nom statique
Name = "ec2-tf-training-${var.STUDENT_NAME}"
# Nom dynamique
Name = "ec2-tf-training-${var.STUDENT_NAME}-${count.index + 1}"
```

Vérifier avec la planification que :

- Une ressource de type `aws_instance` va bien être ajoutée
- Son nom dynamique inclut bien l'index de comptage
- L'instance existante sera mise à jour avec le nouveau nom
- Aucune autre ressource ne va être supprimée ou modifiée

Appliquer les modifications.

Récupérer l'IP de cette nouvelle VM et vérifier qu'il est possible de s'y connecter en SSH avec le compte ec2-user. Comme vu précédemment, il est possible d'utiliser la commande suivante : `terraform show -json | jq '.values.root_module.resources[] | select(.address=="aws_instance.vm")'`
