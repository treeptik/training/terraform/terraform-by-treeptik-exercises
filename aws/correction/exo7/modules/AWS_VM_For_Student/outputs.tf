output "IPs" {
  value = aws_instance.vm.*.public_ip
}

output "names" {
  value = aws_instance.vm.*.tags.Name
}