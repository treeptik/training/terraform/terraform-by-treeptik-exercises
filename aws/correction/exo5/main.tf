# Configure Terraform
terraform {
  required_version = ">= 0.15.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~>3.0"
    }
  }

  # Backend on AWS
  backend "s3" {
    # Bucket name created before
    bucket               = "training-tf-backend"
    key                  = "<YOUR_NAME>/terraform.tfstate"
    dynamodb_table       = "training-tf-lock"
  }
}