# Datasource to find the latest Amazon Linux 2 AMI ID
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
# Datasource to find the training subnet
data "aws_subnet" "public" {
  tags = {
    Name = "sub-tf-training-pub"
  }
}
# Datasource to find the training security group
data "aws_security_group" "sg" {
  tags = {
    Name = "sg-tf-training"
  }
}

# Resource that will contain your public_key. We will attach this resource on an EC2 instance to be accessible with our SSH key
resource "aws_key_pair" "aws_ec2_key" {
  key_name   = "aws_ec2_key"
  public_key = file("~/.ssh/*.pub")
}

# Resource which will create an aws EC2 instance.
resource "aws_instance" "vm" {
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = "t2.micro"
  subnet_id                   = data.aws_subnet.public.id
  vpc_security_group_ids      = [data.aws_security_group.sg.id]
  key_name                    = aws_key_pair.aws_ec2_key.key_name
  associate_public_ip_address = true

  tags = {
    Name      = "ec2-tf-training"
    Env       = "training"
    Terraform = true
  }
}








