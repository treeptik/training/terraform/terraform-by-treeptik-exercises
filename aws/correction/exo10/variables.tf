variable "public_key" {
  type    = string
  default = "~/.ssh/*.pub"
}

variable "VM_SIZE" {
  type    = string
  default = "t2.micro"
}

variable "STUDENT_NAME" {
  type    = string
  default = "studentX"
}

variable "VM_NUMBER" {
  type    = number
  default = 2
}

variable "env_instance_number" {
  type        = map(number)
  description = "(Optional) Number of instance in the current environment"

  default = {
    "default"  = 2
    "dev"      = 3
    "staging"  = 3
    "prod"     = 4
  }
}