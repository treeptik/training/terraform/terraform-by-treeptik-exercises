
output "ip" {
    value = module.ec2.IPs
}

output "end_date_module_ec2" {
    value = module.ec2.end_date
}