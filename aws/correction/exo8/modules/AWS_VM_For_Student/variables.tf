variable "public_key" {
  type = String
}

variable "VM_SIZE" {
  default = "t2.micro"
}

variable "STUDENT_NAME" {
  default = "studentX"
}

variable "VM_NUMBER" {
  default = 2
}

