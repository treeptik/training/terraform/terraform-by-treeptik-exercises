output "IPs" {
  value = aws_instance.vm.*.public_ip
}

output "names" {
  value = aws_instance.vm.*.tags.Name
}

output "end_date" {
    value = formatdate("MMM DD, YYYY", timestamp())
}