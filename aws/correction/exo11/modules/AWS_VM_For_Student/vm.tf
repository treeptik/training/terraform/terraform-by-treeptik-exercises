# Datasource to find the latest Amazon Linux 2 AMI ID
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
# Datasource to find the training subnet
data "aws_subnet" "public" {
  tags = {
    Name = "sub-tf-training-pub"
  }
}
# Datasource to find the training security group
data "aws_security_group" "sg" {
  tags = {
    Name = "sg-tf-training"
  }
}

locals {
  project_name      = "tf-training"
  environment_name  = "training"
}

# Resource that will contain your public_key. We will attach this resource on an EC2 instance to be accessible with our SSH key
resource "aws_key_pair" "public_key" {
  key_name   = "aws_ec2_key"
  public_key = file(var.public_key)
}

# Resource which will create an aws EC2 instance.
resource "aws_instance" "vm" {
  count                       = var.VM_NUMBER
  ami                         = data.aws_ami.amazon-linux-2.id
  instance_type               = var.VM_SIZE
  subnet_id                   = data.aws_subnet.public.id
  vpc_security_group_ids      = [data.aws_security_group.sg.id]
  key_name                    = aws_key_pair.public_key.key_name
  associate_public_ip_address = true
  
  tags = {
    Name      = "ec2-${local.project_name}-${var.STUDENT_NAME}-${count.index + 1}"
    Env       = local.environment_name
    Terraform = true
  }
}








