# Configure Terraform
terraform {
  required_version = ">= 0.15.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~>3.0"
    }
    local = {}
    template = {}
  }

  # Backend on AWS
  backend "s3" {
    # Bucket name created before
    bucket               = "training-tf-backend"
    key                  = "<YOUR_NAME>/terraform.tfstate"
    dynamodb_table       = "training-tf-lock"
  }
}

locals {
  environment     = terraform.workspace
  instance_number = lookup(var.env_instance_number, terraform.workspace)
}

module "ec2" {
  source        = "./modules/AWS_VM_For_Student"
  VM_SIZE       = var.VM_SIZE
  public_key    = var.public_key
  STUDENT_NAME  = var.STUDENT_NAME
  VM_NUMBER     = local.instance_number
}