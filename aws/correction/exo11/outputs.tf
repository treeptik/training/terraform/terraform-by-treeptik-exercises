output "IPs" {
  value = module.ec2.IPs
}

output "names" {
  value = module.ec2.names
}

output "end_date" {
    value = formatdate("MMM DD, YYYY", timestamp())
}

data "template_file" "ansible" {
  template = file("./templates/hosts.tpl")

  vars = {
    connection_string_vm = join("\n", formatlist("%s ansible_host=%s", module.ec2.names, module.ec2.IPs))
  }
}

resource "local_file" "ansible" {
  content  = data.template_file.ansible.rendered
  filename = "./hosts"
}