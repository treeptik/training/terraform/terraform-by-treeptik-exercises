output "instances" {
  value = {
    public_ips = aws_instance.vm.*.public_ip
    names = aws_instance.vm.*.tags.Name
  }
}