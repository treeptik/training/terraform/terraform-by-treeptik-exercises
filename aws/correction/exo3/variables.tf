variable "public_key" {
  type    = string
  default = "~/.ssh/*.pub"
}

variable "VM_SIZE" {
  type    = string
  default = "t2.micro"
}

variable "STUDENT_NAME" {
  type    = string
  default = "studentX"
}
