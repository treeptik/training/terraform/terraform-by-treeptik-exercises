# Exercice 11 : Générer un inventaire Ansible

Dans cet exercice nous allons utiliser les templates et les outputs pour demander à Terraform de générer automatiquement l'inventaire Ansible contenant nos VMs.

Ajouter les providers suivants à la liste des `required_providers` dans la configuration de Terraform :

- local
- template

Créer le répertoire `./templates` et créer dedans le fichier template `hosts.tpl` avec le contenu suivant :

```ini
[all]
${connection_string_vm}
```

Ajouter au fichier `outputs.tf` le contenu suivant :

```yaml
data "template_file" "ansible" {
  template = file("./templates/hosts.tpl")

  vars = {
    connection_string_vm = join("\n", formatlist("%s ansible_host=%s", module.ec2.names, module.ec2.IPs))
  }
}

resource "local_file" "ansible" {
  content  = data.template_file.ansible.rendered
  filename = "./hosts"
}
```

Réinitialiser le projet avec `terraform init` pour installer les nouveaux providers.

Appliquer ensuite les modifications avec la commande `terraform apply` comme s'il s'agissait d'un nouveau composant d'infrastructure.

Afficher l'inventaire Ansbible qui a été créé à la racine du projet avec la commande `cat hosts`. Verifier que vos deux VM ont bien été répertoriées, avec le bon nom et la bonne IP.

Pour bien comprendre comment cela fonctionne, aller voir [la documentation de la fonction `formatlist()`](https://www.terraform.io/docs/configuration/functions/formatlist.html).
