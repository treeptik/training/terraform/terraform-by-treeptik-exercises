# Exercice 3 : Utilisation des variables

## Variabiliser

Pour rendre le code plus propre et plus facile à maintenir nous allons variabiliser les propriétés de la VM.

Créer un fichier `variables.tf` pour accueillir les variables.

Déclarer la variable `VM_SIZE` en positionnant comme valeur par défaut la valeurs précédemment utilisées pour la création de la VM.

Déclarer la variable `public_key` de type String en la valorisant avec le chemin relatif de la clé publique.
La valeur sera passée via un fichier `terraform.tfvars`.

Si besoin s'aider de la [documentation officielle](https://www.terraform.io/docs/configuration/variables.html).

On peut également créer une variable `STUDENT_NAME` pour stocker votre nom.

Modifier le fichier `vm.tf` pour utiliser ces nouvelles variables. Vous pouvez modifier le tag `Name` pour intégrer votre nom par exemple: `ec2-tf-training-${var.STUDENT_NAME}`.

Tester la syntaxe et vérifier dans la planification que toutes les variables ont bien été utilisées.

Normalement la commande `terraform plan` doit renvoyer une liste de modifications en adéquation avec les changements que vous avez fait, c'est important de bien le vérifier.

## Surcharger

Créer un fichier `terraform.tfvars` à la racine du projet afin de tester la surcharge de variable.

Dans le fichier, surcharger la variable `STUDENT_NAME` par une autre valeur.

Tester la planification et vérifier que la nouvelle valeur a bien été prise en compte.
