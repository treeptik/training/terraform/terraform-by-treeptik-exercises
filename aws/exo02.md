# Exercice 2 : Créer des ressources

## Créer une instance EC2

Nous allons créer une première VM sur AWS.

Créer un nouveau fichier `vm.tf` dans votre projet et ajouter la création d'une instance EC2 (nom d'une machine virtuelle chez AWS).

Vous pouvez vous aider de la [documentation officielle](https://www.terraform.io/docs/providers/aws/r/instance.html).

Vous allez avoir besoin de préciser l'identifiant d'une AMI (image), pour cela nous pouvons utiliser la datasource suivante:

```hcl
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
```

Et la resource pour l'instance EC2:

```hcl
# Resource which will create an aws EC2 instance.
resource "aws_instance" "vm" {
  ami                     = data.aws_ami.amazon-linux-2.id
  instance_type           = "t2.micro"
  associate_public_ip_address=true
  tags = {
    Name      = "ec2-tf-training"
    Env       = "training"
    Terraform = true
  }
}
```

Valider la synthaxe avec `terraform validate`.
Etudier la planification avec `terraform plan`.
Si tout semble correct exécuter le `terraform apply`.

Qu'observez-vous ?

- l'instance a-t-elle pu être créée ?
- dans quel VPC est-elle ?

Créer les datasources permettant de trouver:

- Le Subnet qui possède un tag `Name` avec la valeur `sub-tf-training-pub`
- Le Security Group qui possède un tag `Name` avec la valeur `sg-tf-training`

Ajouter les propriétés nécessaires à votre instance pour la déployer dans le subnet de la formation et avec le security group.

Vérifiez la synthaxe et tenter à nouveau de créer ou modifier l'instance avec `terraform apply`.

## Se connecter sur l'instance

Comment faire pour se connecter en SSH à cette nouvelle VM ?

Nous allons créer une ressource `aws_key_pair` contenant la clé publique de la formation et l'ajouter à notre instance, elle sera automatiquement ajoutée à l'utilisateur par défaut soit `ec2-user` pour les AMI Amazon Linux.

Pour afficher le contenu de la clé publique: `cat ~/.ssh/*.pub`
Sinon on peut utiliser la fonction `file()` qui permet d'obtenir le contenu d'un fichier. Voici ce que pourrait être la ressource à créer:

```hcl
# Resource that will contain your public_key. We will attach this resource on an EC2 instance to be accessible with our SSH key
resource "aws_key_pair" "aws_ec2_key" {
  key_name   = "aws_ec2_key"
  public_key = file("~/.ssh/*.pub")
}
```

Mais comment obtenir l'IP de la VM ?

On peut obtenir les informations de l'infrastructure provisionnée avec la commande `terraform show`.

Pour regarder plus spécifiquement l'instance générée : `terraform show -json | jq '.values.root_module.resources[] | select(.address=="aws_instance.vm")'`

Récupérer l'IP de la VM et essayer de se connecter dessus avec le compte `ec2-user`. Normalement la connexion se fait automatiquement avec la paire de clé SSH sans demander de mot de passe.

```bash
ssh ec2-user@<IP_PUBLIQUE>
```
