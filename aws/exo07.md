# Exercice 7 : Module

## Créer un module

Dans cet exercice, nous allons apprendre le concept de "module" dans terraform. En terme de code, on pourrait comparer les modules à des fonctions réutilisables.

Une bonne pratique pour la création de module est de créer un sous répertoire "modules" pour y placer les modules :

* Répertoire Projet
  * main.tf
  * variables.tf
  * modules/
    * Nom_Module[1]/
      * main.tf
      * variables.tf
      * outputs.tf
    * Nom_Module[2]/
      * main.tf
      * variables.tf
      * outputs.tf
    * ...

Créer le répertoire `modules`, rajouter le sous répertoire `AWS_VM_For_Student` et déplacer les fichiers `vm.tf` et `outputs.tf` dans ce sous répertoire et copier le fichier `variables.tf` :

* Répertoire Projet
  * main.tf
  * provider.tf
  * variables.tf
  * modules/
    * AWS_VM_For_Student/
      * vm.tf
      * variables.tf
      * outputs.tf

Dans le fichier `main.tf`, rajouter l'appel au module que l'on vient de créer :

```bash
module "ec2" {
  source        = "./modules/AWS_VM_For_Student"
  VM_SIZE       = var.VM_SIZE
  public_key    = var.public_key
  STUDENT_NAME  = var.STUDENT_NAME
  VM_NUMBER     = var.VM_NUMBER
}
```

On constate l'attribut `source` qui pointe sur le module en question et les autres attributs qui ne sont autres que les variables spécifiées dans le fichier `./modules/AWS_VM_For_Student/variables.tf`.

Déplacer les variables locales du `main.tf` dans le fichier `vm.tf` du module.

Jouez les étapes suivantes :

* `terraform init` pour charger les modules
* `terraform plan` pour voir les changements prévus

Constater que Terraform va supprimer les anciennes ressources pour les recréer avec le module car l'identifiant des objets change:
`aws_instance.vm[1] -> module.ec2.aws_instance.vm[1]`

* `terraform apply` pour appliquer les modifications

## Retourner des valeurs

Notre module possède un fichier `outputs.tf` et renvoit des valeurs, mais notre projet principal ne les exploite pas pour le moment.

Pour récupérer la sortie du module dans un autre module, il suffira alors d'utiliser la syntaxe suivante : `module.[MODULE NAME].[OUTPUT NAME]`

Créer à présent le fichier `outputs.tf` à la racine du projet et renseigner :

```bash
output "IPs" {
  value = module.ec2.IPs
}
output "names" {
  value = module.ec2.names
}
```

Jouer la commande : `terraform apply` ==> Le retour s'affiche à présent.
