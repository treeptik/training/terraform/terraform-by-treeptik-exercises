# Exercice 10 : Fonctions utilitaires

Le langage HCL offre différentes fonctions que l'on peut utiliser dans les expressions pour transformer ou combiner des valeurs.
[Voici la documentation complète des fonctions](https://www.terraform.io/docs/configuration/functions.html)

## Calculer la date

Ici nous allons simplement utiliser la fonction [formatdate(spec, timestamp)](https://www.terraform.io/docs/configuration/functions/formatdate.html) avec la fonction [timestamp()](https://www.terraform.io/docs/configuration/functions/timestamp.html) et nous permettre d'afficher la date de fin d'execution de notre script terraform.

Pour cela dans le fichier `outputs.tf` du module `AWS_VM_For_Student`, ajouter le output suivant:

```bash
output "end_date" {
    value = formatdate("MMM DD, YYYY", timestamp())
}
```

Pensez à le rajouter également dans le fichier `outputs.tf` à la racine du projet:

``` bash
output "end_date_module_ec2" {
    value = module.ec2.end_date
}
```

Jouez la commande `terraform apply` et remarquer la sortie à la fin de l'éxécution ==> date formattée.

## Autre

Utiliser des variables locales et des outputs pour tester les fonctions très utiles au quotidien:

- Manipulation des chaînes de caractères
- Manipulation des collections
