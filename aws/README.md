# Terraform by Treeptik - Exercices

## Prérequis

Pour réaliser ces exercices vous devez avoir :

- Un serveur d'admin sur lequel est installé Terraform
- Le mot de passe du compte `training` pour se connecter au serveur d'admin distant
- Une secret key AWS

Les serveurs d'admin mis a disposition de chaque participant ont été initialisés lors du provisionnement par Terraform/Ansible.

## Exercices

Tous les exercices sont réalisés à partir du serveur d'administration :

- [Exercice 01 : Mise en oeuvre](./exo01.md)
- [Exercice 02 : Créer des ressources](./exo02.md)
- [Exercice 03 : Utilisation des variables](./exo03.md)
- [Exercice 04 : Les Boucles](./exo04.md)
- [Exercice 05 : Backend](./exo05.md)
- [Exercice 06 : Locals et outputs](./exo06.md)
- [Exercice 07 : Module](./exo07.md)
- [Exercice 08 : Création de workspace](./exo08.md)
- [Exercice 09 : Cycle de vie](./exo09.md)
- [Exercice 10 : Fonctions utilitaires](./exo10.md)
- [Exercice 11 : Générer un inventaire Ansible](./exo11.md)
- [Exercice 12 : Destruction de l'infrastructure](./exo12.md)

## Correction

Les corrections sont disponibles [ici](./correction).
