# Exercice 6 : Locals et output

## Locals

Dès qu'une valeur est utilisée plusieurs fois dans le projet, il peut être intéressant d'en faire une variable locale.

Dans le fichier `main.tf`, ajouter des variables locales pour stocker les informations que l'on utilise dans les tag des ressources:

- le nom du projet `tf-training`
- le nom de l'environnement `training`

```bash
locals {
  project_name      = "tf-training"
  environment_name  = "training"
}
```

Dans le fichier `vm.tf`, utiliser ces variables locales dans les tags.

Verifier avec `terraform plan` que cela n'engendre pas de modification sur l'infrastructure.

## Outputs

Il est intéressant d'afficher certaines valeurs en sortie de la stack. Dans le cas de notre projet on pense notamment aux IP public de nos instances par exemple, afin d'éviter de devoir aller les chercher dans le fichier d'état qui est assez verbeux.

Créer un fichier `outputs.tf` contenant l'output suivant:

```bash
output "instances" {
  value = {
    public_ips = aws_instance.vm.*.public_ip
    names = aws_instance.vm.*.tags.Name
  }
}
```

Appliquer les modifications avec `terraform apply`, vous devriez voir s'afficher l'output.

On peut aussi afficher l'ensemble des outputs du projet avec `terraform output`.
