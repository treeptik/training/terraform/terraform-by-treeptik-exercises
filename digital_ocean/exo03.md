# Exercice 3 : Utilisation des variables

## Variabiliser

Pour rendre le code plus propre et plus facile à maintenir nous allons variabiliser les propriétés de la VM.

Créer un fichier `variables.tf` pour accueillir les variables.

Déclarer la liste de variables suivante en positionnant comme valeur par défaut les valeurs précédemment utilisées pour la création de la VM :

- `REGION`
- `VM_IMAGE`
- `VM_SIZE`
- `VM_SSH_KEYS`

Si besoin s'aider de la [documentation officielle](https://www.terraform.io/docs/configuration/variables.html).

On peut également créer une variable pour stocker votre identifiant de student (studentX) :

- `STUDENT_NAME`

Modifier le fichier `vm.tf` pour utiliser toutes ces nouvelles variables.

Pour la propriété `name` de la VM vous devriez avoir quelque chose de la forme :

```yaml
name = "${var.STUDENT_NAME}-1"
```

Tester la syntaxe et vérifier dans la planification que toutes les variables ont bien été utilisées.

Normalemt la commande `terraform plan` doit indiquer qu'il n'y a aucune modification à appliquer sur l'infrastructure.

## Surcharger

Créer un fichier `terraform.tfvars` à la racine du projet afin de tester la surcharge de variable.

Dans le fichier, surcharger la variable `STUDENT_NAME` par une autre valeur.

Tester la planification (sans appliquer) et vérifier que la nouvelle valeur a bien été prise en compte.

Supprimer le fichier `terraform.tfvars`.
