# Exercice 1 : Initialisation d'un projet

## Vérifier l'environnement

Commencer par vérifier que l'environnement est correctement configuré.

Vérifier que les variables d'environnement suivantes sont bien présentes avec la commande `env` :

- `DIGITALOCEAN_TOKEN`
- `SPACES_ACCESS_KEY_ID`
- `SPACES_SECRET_ACCESS_KEY`.

Vérifier que les clés SSH `sophiaconf_rsa` et `sophiaconf_rsa.pub` sont bien présentes dans `~/.ssh`.

Vérifier que Terraform est bien installé en testant la commande `terraform`. En profiter pour consulter les différentes commandes de la CLI Terraform.

Vérifier que le projet de ce workshop a bien été cloné dans `~/sophiaconf2019-terraform-workshop`.

Vérifier que le fichier de variable `~/training-variable.txt` est bien présent.

## Initialiser le projet

Créer un répertoire `sophiaconf` pour accueillir vos travaux et se déplacer dedans.

Créer un fichier `main.tf` avec la configuration de Terraform, indiquer que le projet utilise une version supérieure à `0.12.0`. Si besoin consulter [la documentation officielle](https://www.terraform.io/docs/configuration/terraform.html#specifying-a-required-terraform-version).

Créer un fichier `provider.tf` pour déclarer le provider `digitalocean`. Si besoin consulter [la documentation officielle](https://www.terraform.io/docs/providers/do/index.html).
On s'appuiera sur les variables d'environnement pour les credentials.

Valider la syntaxe puis initialiser le projet avec la CLI Terraform.
