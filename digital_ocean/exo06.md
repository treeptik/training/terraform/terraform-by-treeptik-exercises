# Exercice 6 : Modularisation du code

Dans cet exercice, nous allons apprendre le concept de "module" dans terraform. En terme de code, on pourrait comparer les modules à des fonctions réutilisables. 


Une bonne pratique pour la création de module est de créer un sous répertoire "modules" pour y placer les modules : 

* Répertoire Projet
  * main.tf
  * variables.tf
  * Modules/
    * Nom_Module[1]/
      * Main.tf
      * variables.tf
    * Nom_Module[2]/
      * Main.tf
      * variables.tf
    * ... 

Créer le répertoire `modules`, rajouter le sous répertoire `digitalOcean_VM_For_Student` et déplacer les fichiers `vm.tf` `variables.tf` et `output.tf` dans ce sous répertoire :

* Répertoire Projet
  * main.tf
  * provider.tf
  * Modules/
    * digitalOcean_VM_For_Student/
      * vm.tf
      * variables.tf
      * output.tf


Dans le fichier main.tf, rajouter l'appel au module que l'on vient de créer :

``` t

module "droplets" {

  source = "./modules/digitalOcean_VM_For_Student"

  REGION ="fra1"

  VM_IMAGE ="ubuntu-18-04-x64"

  VM_SIZE = "s-1vcpu-1gb"

  VM_SSH_KEYS = "2b:30:e2:cf:12:13:e2:b3:42:35:23:e7:9a:51:82:de"
  
  STUDENT_NAME = "studentX"
  
  VM_NUMBER = 2
}

```

On constate l'attribut `source` qui pointe sur le module en question et les autres attributs qui ne sont autres que les variables spécifiées dans le fichier `./modules/digitalOcean_VM_For_Student/variables.tf`.  


Jouez les étapes suivantes :
* `terraform init` pour charger les modules
* `terraform apply` pour déployer l'infrastructure
* Modifier la variable `VM_NUMBER` dans le module 
* Lancer à nouveau `terraform apply`
  * Remarquer que le fichier hosts est recréé.





