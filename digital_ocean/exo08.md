# Exercice 8 : Création de workspace 



Terraform démarre avec un seul workspace nommé "default". Ce workspace est spécial à la fois parce qu'il est celui par défaut et aussi parce qu'il ne peut jamais être supprimé. Si vous n'avez jamais utilisé explicitement des workspaces, alors vous avez toujours travaillé sur le workspace "default".

Les workspaces sont gérés avec l'ensemble des commandes du workspace terraform : 
* Pour créer un nouvel workspace et y basculer, vous pouvez utiliser `terraform workspace new`
* Pour changer d'espace de travail, vous pouvez utiliser `terraform workspace select`

Par exemple pour créer un nouveau workspace : 

```
$ terraform workspace new bar
Created and switched to workspace "bar"!


You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.

``` 

Dans notre cas, nous allons générer une variable `map` permettant de spécifier une valeur pour tous les environnements.   
Rajouter un fichier variables.tf à la racine du projet et y rajouter la variable suivante :

``` t

variable "env_instance_number" {
  type        = "map"
  description = "(Optional) Number of instances"

  default = {
    "dev"      = 1
    "test"     = 2
    "staging"  = 3
    "prod"     = 6
  }
}

```

Cette variable permettra de définir en fonction de l'environnement sélectionner, le nombre d'instances à lancer. 

Dans le fichier main.tf, définissez les variables `locals` à utiliser dans le projet :

``` t 

locals {

     environment = "${terraform.workspace}"

     instance_number = "${lookup(var.env_instance_number, terraform.workspace)}"

}

```

On utilisera alors la variable `instance_number` dans le projet qui sera setter ici en fonction de l'environnement sélectionné. 

Modifier ensuite dans l'appel au module `droplets` la variable : 

``` t

# Qui fait référence à la variable local
VM_NUMBER = "${local.instance_number}"

```

Créer ensuite vos environnements : 

``` t

terraform workspace new dev
terraform workspace new test
terraform workspace new staging
terraform workspace new prod


```

Pour lister vos différents workspace, vous pouvez appliquer la commande `terraform workspace list`.  

Sélectionner l'environnement de test avec `terraform workspace select test` et jouer la commande `terraform plan`.   

Remarquer que le nombre d'instances à créer est de 2.  

Sélectionner maintenant l'environnement de prod  avec `terraform workspace select prod` et jouer la commande `terraform plan`.   

Remarquer que le nombre d'instances à créer est de 6.  
