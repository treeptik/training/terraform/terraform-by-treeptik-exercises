# Exercice 5 : Générer un inventaire Ansible

Dans cet exercice nous allons utiliser les templates et les outputs pour demander à Terraform de générer automatiquement l'inventaire Ansible contenant nos VMs.

Ajouter les providers suivants dans le fichier `provider.tf` :

```yaml
provider "local" {
  version = "~> 1.2.2"
}

provider "template" {
  version = "~> 2.1.2"
}
```

Créer le répertoire `./templates` et créer dedans le fichier `hosts.tpl` avec le contenu suivant :

```ini
[all]
${connection_string_vm}
```

Créer un fichier `output.tf` avec le contenu suivant :

```yaml
data "template_file" "ansible" {
  template = "${file("./templates/hosts.tpl")}"

  vars = {
    connection_string_vm = "${join("\n", formatlist("%s ansible_host=%s", digitalocean_droplet.vm.*.name ,  digitalocean_droplet.vm.*.ipv4_address))}"
  }
}

resource "local_file" "ansible" {
  content  = "${data.template_file.ansible.rendered}"
  filename = "./hosts"
}
```

Réinitialiser le projet avec `terraform init` pour initialiser les nouveaux providers.

Appliquer ensuite les modifications avec la commande `terraform apply` comme s'il s'agissait d'un nouveau composant d'infrastructure.

Afficher l'inventaire Ansbible avec la commande `cat hosts`. Verifier que vos deux VM ont bien été répertoriées.
