data "template_file" "ansible" {
  template = "${file("./templates/hosts.tpl")}"

  vars = {
    connection_string_vm = "${join("\n", formatlist("%s ansible_host=%s", digitalocean_droplet.vm.*.name ,  digitalocean_droplet.vm.*.ipv4_address))}"
  }
}

resource "local_file" "ansible" {
  content  = "${data.template_file.ansible.rendered}"
  filename = "./hosts"
}

output "instance_ip_addr" {
    value = digitalocean_droplet.vm.*.ipv4_address
}