# Configure Terraform
terraform {
  required_version = ">= 0.12.0"
}

provider "local" {
  version = "~> 1.2.2"
}

provider "template" {
  version = "~> 2.1.2"
}


module "droplets" {

  source = "./modules/digitalOcean_VM_For_Student"

  REGION ="fra1"

  VM_IMAGE ="ubuntu-18-04-x64"

  VM_SIZE = "s-1vcpu-1gb"

  VM_SSH_KEYS = "2b:30:e2:cf:12:13:e2:b3:42:35:23:e7:9a:51:82:de"
  
  STUDENT_NAME = "studentX"
  
  VM_NUMBER = "${local.instance_number}"
}

locals {

     environment = "${terraform.workspace}"

     instance_number = "${lookup(var.env_instance_number, terraform.workspace)}"


}