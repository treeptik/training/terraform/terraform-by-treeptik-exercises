# Create Droplet master for each student
resource "digitalocean_droplet" "vm" {
  count    = "${var.VM_NUMBER}"
  name     = "${var.STUDENT_NAME}-${count.index + 1}"
  image    = "${var.VM_IMAGE}"
  region   = "${var.REGION}"
  size     = "${var.VM_SIZE}"
  ssh_keys = ["${var.VM_SSH_KEYS}"]
}
