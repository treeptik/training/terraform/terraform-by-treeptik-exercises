
output "ip" {
    value = module.droplets.instance_ip_addr
}


output "end_date_module_droplet" {
    value = module.droplets.end_date
}