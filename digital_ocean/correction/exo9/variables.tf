variable "env_instance_number" {
  type        = "map"
  description = "(Optional) Number of instances"

  default = {
    "dev"      = 1
    "test"     = 2
    "staging"  = 3
    "prod"     = 6
  }
}
