# Configure Terraform
terraform {
  required_version = ">= 0.12.0"
}

provider "local" {
  version = "~> 1.2.2"
}

provider "template" {
  version = "~> 2.1.2"
}