# Exercice 10 : Destruction de l'infrastructure

Détruire l'infrastructure avec la commande `terraform destroy`.

Vérifier avec `terraform show` qu'il ne reste plus rien.

Constater que le fichier d'inventaire `./hosts` a disparu au même titre que les ressources d'infrastructure.
