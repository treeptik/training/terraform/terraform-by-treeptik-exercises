# Exercice 7 : Retourner des valeurs avec les modules

Dans cet exercice, nous allons voir qu'il est possible de retourner les valeurs du module qu'il est nécessaire de retourner pour être utilisable dans d'autres modules. 


Pour cela nous avons comme dans du code besoin de spécifier un `return` à notre `fonction`. Ici, nous avons besoin de spécifier un `output` à notre `module`.

Pour récupérer la sortie du module dans un autre module, il suffira alors d'utiliser la syntaxe suivante : `module.[MODULE NAME].[OUTPUT NAME]`


Pour cela rajouter le output suivant dans le fichier `./modules/digitalOcean_VM_For_Student/output.tf` :

``` t

output "instance_ip_addr" {
    value = digitalocean_droplet.vm.*.ipv4_address
}

```

Jouer la commande : `terraform apply` ==> Remarquer que le output ne s'affiche pas.  

Créer à présent le fichier `output.tf` à la racine du projet et renseigner :

``` t

output "ip" {
    value = module.droplets.instance_ip_addr
}

``` 
Jouer la commande : `terraform apply` ==> Le retour s'affiche à présent. 
