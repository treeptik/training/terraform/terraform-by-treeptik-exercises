# Exercice 2 : Création d'une VM

Nous allons créer une première VM sur Digital Ocean.

Créer un nouveau fichier `vm.tf` et ajouter la création d'une droplet (nom d'une VM chez Digital Ocean) avec les propriétés suivantes :

- image : `ubuntu-18-04-x64`
- région : `fra1`
- taille : `s-1vcpu-1gb`
- clé SSH : récupérer la valeur de la variable `SSH_KEY_FINGERPRINT` dans le fichier de variable `~/training-variable.txt`
- nom : `studentX-1` avec X correspondant au numéro de votre serveur d'admin

Utiliser `vm` comme nom de ressource, c'est important nous nous en reservirons plus tard.

Si besoin s'aider de la [documentation officielle](https://www.terraform.io/docs/providers/do/r/droplet.html).

Valider la synthaxe avec `terraform validate`.

Etudier la planification avec `terraform plan`.

Si tout semble correct créer l'infrastructure avec `terraform apply`.

Comment faire pour se connecter à cette nouvelle VM ?

Grâce à la propriété `ssh_keys` la clé publique a automatiquement été ajoutée pour le compte root.

Mais comment obtenir l'IP de la VM ?

On peut obtenir les informations de l'infrastructure provisionnée avec la commande `terraform show`.

Récupérer l'IP de la VM et essayer de se connecter dessus avec le compte root. Normalement la connexion se fait automatiquement avec la paire de clé SSH sans demander de mot de passe.
