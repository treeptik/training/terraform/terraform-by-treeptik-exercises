# Exercice 9 : Fonctions utilitaires


Le langage HCL offre différentes fonctions que l'on peut utiliser dans les expressions pour transformer ou combiner des valeurs.  

Ici nous allons simplement utiliser la fonction [formatdate(spec, timestamp)](https://www.terraform.io/docs/configuration/functions/formatdate.html) avec la fonction [timestamp()](https://www.terraform.io/docs/configuration/functions/timestamp.html) et nous permettre d'afficher la date de fin d'execution de notre script terraform.


Pour cela dans le fichier output.tf du module `digitalOcean_VM_For_Student` rajouter le output suivant : 

``` t

output "end_date" {
    value = formatdate("MMM DD, YYYY", timestamp())
}

```

Pensez à le rajouter également dans le output.tf à la racine du projet :

``` t

output "end_date_module_droplet" {
    value = module.droplets.end_date
}

```


Jouez la commande `terraform apply` et remarquer la sortie à la fin de l'éxécution ==> date formattée.


