# Terraform by Treeptik - Exercices

## Prérequis

Pour réaliser ces exercices vous devez avoir :

- Un serveur d'administration sur lequel est installé Terraform
- La clé privée SSH
- Un token Digital Ocean

Les serveurs d'admin mis a disposition de chaque participant ont été initialisés lors du provisionnement par Ansible.

## Exercices

Tous les exercices sont réalisés à partir du serveur d'administration :

- [Exercice1 : Initialisation d'un projet](./exo1.md)
- [Exercice2 : Création d'une VM](./exo2.md)
- [Exercice3 : Utilisation des variables](./exo3.md)
- [Exercice4 : Modification de l'infrastructure](./exo4.md)
- [Exercice5 : Générer un inventaire Ansible](./exo5.md)
- [Exercice6 : Destruction de l'infrastructure](./exo6.md)

## Correction

Les corrections sont disponible [ici](./correction).
